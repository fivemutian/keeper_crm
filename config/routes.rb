Rails.application.routes.draw do

  root 'home#index'
  # sign_up 
  post    'sign_up' => "registrations#create" 
  # sign_in
  get 		'login' => 'sessions#new'
  post 		'login' => 'sessions#create'
  delete 	'logout' => 'sessions#destroy'

  namespace :api do
    namespace :wechat do
      post :auth
    end
    post "upload" => "base#upload"
    resources :valid_codes, only: [:create] do
      collection do
        post :valid
      end
    end
  	resources :registrations, only: [:create]
  	resources :sessions, only: [:create]

    namespace :auth do
      get :cgj
    end
    
    namespace :sync do
      resources :orders, only: [] do
        collection do
          post :update_cgj
        end
      end
      resources :customers, only: [:create]
      resources :accounts, only: [] do
        collection do
          post :gen_account_user
          post :update_cgj
        end
      end
    end

	  resources :users do # 用户管理
      collection do
        post :update_me
        post :update_password
        post :reset_password
        get  :bank_cards
        post :add_bank_card
        post :update_bank_card
        get  :statistics
        post :update_me_wx # 小程序
      end
    end
	  resources :clues do # 线索管理
      collection do
        post :update_me
        post :assign
        post :add_remark
      end
    end

    resources :regions, only: [:index, :create, :show] do
      resources :stores, only: [:index, :create, :update]
      resources :region_strategies, only: [:index, :create, :update, :destroy]
    end
    post "stores/:id" => "stores#update"
  	
    post "orders/:id/upload_file" => "orders#upload_file"
    post "orders/:id/check_order" => "orders#check_order"
    post "orders/:id/delete_pictures" => "orders#delete_pictures"
  	resources :orders do # 订单管理
      collection do
        get :get_cgjs
        post :update_estimated_total
        post :update_rebate
        get :fetch_users
      end
    end
  	resources :customers do # 客户管理 
      member do
        post :update
      end
    end
    resources :strategies do # 策略管理
      member do
        post :update
      end
    end
    resources :accounts do
      collection do
        get :get_directors
        get :companies
        post :add_company
        post :apply_co_companies
        post :update_apply
        post :update_me
      end
    end
    resources :pres, only: [:index]

    resources :wallets, only: [] do
      collection do
        get :me
        get  :config
        post :recharge
        post :withdraw
      end
    end

    resources :strategy_results, only: [:index, :show] do
      member do
        post :update
        post :pay
      end
    end

    resources :suggestions
	end

  resources :payments, only: [] do
    collection do
      post :callback
    end
  end

  namespace :admin do
    resources :users
    resources :orders
    resources :clues
    resources :wallet_logs, only: [:index] do
      member do
        put :confirm_pay
      end
    end
    resources :suggestions
  end

end
