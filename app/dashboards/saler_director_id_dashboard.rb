require "administrate/base_dashboard"

class SalerDirectorIdDashboard < Administrate::BaseDashboard
  def display_resource(user_id)
    User.find_by(id: user_id).try(:name)
  end
end
