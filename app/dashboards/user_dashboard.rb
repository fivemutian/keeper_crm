require "administrate/base_dashboard"

class UserDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    account: Field::BelongsTo,
    token: Field::HasOne,
    clues: Field::HasMany,
    assign_clues: Field::HasMany.with_options(class_name: "Clue"),
    orders: Field::HasMany,
    customers: Field::HasMany,
    permissions: Field::HasMany,
    wallet_logs: Field::HasMany,
    applies: Field::HasMany,
    bank_cards: Field::HasMany,
    children: Field::HasMany.with_options(class_name: "User"),
    id: Field::Number,
    mobile: Field::String,
    password_digest: Field::String,
    remeber_digest: Field::String,
    name: Field::String,
    role: Field::String,
    status: Field::BelongsTo,
    open_id: Field::String,
    cgj_user_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    saler_director_id: Field::BelongsTo,
    avatar: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :mobile,
    :role,
    :account,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :account,
    # :token,
    # :clues,
    # :assign_clues,
    # :orders,
    # :customers,
    # :permissions,
    # :wallet_logs,
    # :applies,
    # :bank_cards,
    # :children,
    :id,
    :mobile,
    # :password_digest,
    # :remeber_digest,
    :name,
    :role,
    :status,
    :open_id,
    :cgj_user_id,
    :created_at,
    :updated_at,
    :saler_director_id,
    :avatar,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :account,
    # :token,
    # :clues,
    # :assign_clues,
    # :orders,
    # :customers,
    # :permissions,
    # :wallet_logs,
    # :applies,
    # :bank_cards,
    # :children,
    :mobile,
    # :password_digest,
    # :remeber_digest,
    :name,
    :role,
    :status,
    # :open_id,
    :cgj_user_id,
    :saler_director_id,
    # :avatar,
  ].freeze

  # Overwrite this method to customize how users are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(user)
    # "User ##{user.id}"
    user.name
  end
end
