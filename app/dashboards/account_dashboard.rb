require "administrate/base_dashboard"

class AccountDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    children: Field::HasMany.with_options(class_name: "Company"),
    users: Field::HasMany,
    regions: Field::HasMany,
    stores: Field::HasMany,
    orders: Field::HasMany,
    customers: Field::HasMany,
    clues: Field::HasMany,
    admin: Field::HasOne,
    saler_directors: Field::HasMany.with_options(class_name: "User"),
    strategies: Field::HasMany,
    id: Field::Number,
    name: Field::String,
    code: Field::String,
    type: Field::String,
    reg_address: Field::String,
    address: Field::String,
    company_ids: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    parent_id: Field::Number,
    logo: Field::String,
    cgj_id: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :children,
    :users,
    :regions,
    :stores,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :children,
    :users,
    :regions,
    :stores,
    :orders,
    :customers,
    :clues,
    :admin,
    :saler_directors,
    :strategies,
    :id,
    :name,
    :code,
    :type,
    :reg_address,
    :address,
    :company_ids,
    :created_at,
    :updated_at,
    :parent_id,
    :logo,
    :cgj_id,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :children,
    :users,
    :regions,
    :stores,
    :orders,
    :customers,
    :clues,
    :admin,
    :saler_directors,
    :strategies,
    :name,
    :code,
    :type,
    :reg_address,
    :address,
    :company_ids,
    :parent_id,
    :logo,
    :cgj_id,
  ].freeze

  # Overwrite this method to customize how accounts are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(account)
    # "Account ##{account.id}"
    "#{account.name}(#{account.type})"
  end
end
