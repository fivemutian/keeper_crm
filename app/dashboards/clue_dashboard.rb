require "administrate/base_dashboard"

class ClueDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    account: Field::BelongsTo,
    user: Field::BelongsTo,
    customer: Field::BelongsTo,
    assign_user: Field::BelongsTo.with_options(class_name: "User"),
    id: Field::Number,
    name: Field::String,
    mobile: Field::String,
    state: Field::Boolean,
    address: Field::String,
    remark: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    assign_user_id: Field::Number,
    pound: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :mobile,
    :state,
    :user,
    :assign_user,
    :account,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :account,
    :user,
    :customer,
    :assign_user,
    :id,
    :name,
    :mobile,
    :state,
    :address,
    :remark,
    :created_at,
    :updated_at,
    :assign_user_id,
    :pound,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :account,
    :user,
    :customer,
    :assign_user,
    :name,
    :mobile,
    :address,
    :remark,
    :assign_user_id,
    :pound,
  ].freeze

  # Overwrite this method to customize how clues are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(clue)
  #   "Clue ##{clue.id}"
  # end
end
