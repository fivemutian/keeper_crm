require "administrate/base_dashboard"

class StrategyResultDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    saler_id: Field::Number,
    saler_rate_amount: Field::Number.with_options(decimals: 2),
    customer_id: Field::Number,
    customer_discount_amount: Field::Number.with_options(decimals: 2),
    introducer_id: Field::Number,
    introducer_rebate_amount: Field::Number.with_options(decimals: 2),
    remark: Field::String,
    order_id: Field::Number,
    strategy_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    saler_director_id: Field::Number,
    saler_director_rate_amount: Field::Number.with_options(decimals: 2),
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :saler_id,
    :saler_rate_amount,
    :customer_id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :saler_id,
    :saler_rate_amount,
    :customer_id,
    :customer_discount_amount,
    :introducer_id,
    :introducer_rebate_amount,
    :remark,
    :order_id,
    :strategy_id,
    :created_at,
    :updated_at,
    :saler_director_id,
    :saler_director_rate_amount,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :saler_id,
    :saler_rate_amount,
    :customer_id,
    :customer_discount_amount,
    :introducer_id,
    :introducer_rebate_amount,
    :remark,
    :order_id,
    :strategy_id,
    :saler_director_id,
    :saler_director_rate_amount,
  ].freeze

  # Overwrite this method to customize how strategy results are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(strategy_result)
  #   "StrategyResult ##{strategy_result.id}"
  # end
end
