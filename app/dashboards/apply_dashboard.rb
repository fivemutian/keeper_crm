require "administrate/base_dashboard"

class ApplyDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    id: Field::Number,
    target_user_id: Field::Number,
    resource_name: Field::String,
    resource_id: Field::Number,
    _action: Field::String,
    state: Field::Number,
    remark: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :user,
    :id,
    :target_user_id,
    :resource_name,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :id,
    :target_user_id,
    :resource_name,
    :resource_id,
    :_action,
    :state,
    :remark,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :target_user_id,
    :resource_name,
    :resource_id,
    :_action,
    :state,
    :remark,
  ].freeze

  # Overwrite this method to customize how applies are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(apply)
  #   "Apply ##{apply.id}"
  # end
end
