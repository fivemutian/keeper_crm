require "administrate/base_dashboard"

class StoreDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    account: Field::BelongsTo,
    region: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    code: Field::String,
    contact: Field::String,
    phone: Field::String,
    address: Field::String,
    product_ids: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :account,
    :region,
    :id,
    :name,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :account,
    :region,
    :id,
    :name,
    :code,
    :contact,
    :phone,
    :address,
    :product_ids,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :account,
    :region,
    :name,
    :code,
    :contact,
    :phone,
    :address,
    :product_ids,
  ].freeze

  # Overwrite this method to customize how stores are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(store)
    # "Store ##{store.id}"
    store.name
  end
end
