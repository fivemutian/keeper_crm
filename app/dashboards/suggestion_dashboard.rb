require "administrate/base_dashboard"

class SuggestionDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    id: Field::Number,
    _type: Field::Number,
    content: Field::String,
    state: Field::String,
    c_name: Field::String,
    c_tel: Field::String,
    remark: Field::String,
    transactor_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :user,
    :_type,
    :state,
    :content,
    :c_name,
    :c_tel,
    :remark,
    :transactor_id
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :id,
    :_type,
    :content,
    :state,
    :c_name,
    :c_tel,
    :remark,
    :transactor_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :_type,
    :content,
    :state,
    :c_name,
    :c_tel,
    :remark,
    :transactor_id,
  ].freeze

  # Overwrite this method to customize how suggestions are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(suggestion)
  #   "Suggestion ##{suggestion.id}"
  # end
end
