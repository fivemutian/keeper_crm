require "administrate/base_dashboard"

class StatusDashboard < Administrate::BaseDashboard
  def display_resource(number)
    {-1 => '禁用', 0 => '未审核', 1 => '正常'}[number]
  end
end
