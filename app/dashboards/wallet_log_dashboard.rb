require "administrate/base_dashboard"

class WalletLogDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    id: Field::Number,
    transfer: Field::Number,
    state: Field::Number,
    trade_type: Field::String,
    charge_id: Field::String,
    amount: Field::Number.with_options(prefix: '¥', decimals: 2),
    total: Field::Number.with_options(prefix: '¥', decimals: 2),
    strategy_result_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    bank_card_id: Field::Number,
    account_number: Field::String,
    account_bank: Field::String,
    account_branch: Field::String,
    account_name: Field::String,
    paid: Field::Boolean,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :user,
    :amount,
    :account_bank,
    :account_branch,
    :account_number,
    :account_name,
    :paid,
  ].freeze

  COLLECTION_FILTERS = [:withdraw]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :id,
    :transfer,
    :state,
    :trade_type,
    :charge_id,
    :amount,
    :total,
    :strategy_result_id,
    :created_at,
    :updated_at,
    :bank_card_id,
    :account_number,
    :account_bank,
    :account_branch,
    :account_name,
    :paid,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :transfer,
    :state,
    :trade_type,
    :charge_id,
    :amount,
    :total,
    :strategy_result_id,
    :bank_card_id,
    :account_number,
    :account_bank,
    :account_branch,
    :account_name,
    :paid,
  ].freeze

  # Overwrite this method to customize how wallet logs are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(wallet_log)
  #   "WalletLog ##{wallet_log.id}"
  # end
end
