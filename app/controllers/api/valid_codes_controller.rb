# 验证码
class Api::ValidCodesController < Api::BaseController
	skip_before_filter :authenticate_user, :valid_permission

	# 获取
	# 
	# Params
	# 	mobile: [String] 手机号
	# 	type: [Integer] 1注册|2密码重置，default 1
	# Return
	# 	status: [String] success
	# 	msg: [String] 已发送到您手机，请验证
	# Error
	#   status: [String] failed
	#   msg: [String] msg_infos
	def create
		type  = params[:type] ? params[:type].to_i : 1

		if type == 2
			user = User.find_by_mobile(params[:mobile])
			rraise '查无此用户，请重新输入' unless user
		end

		vcode = ValidCode.where(mobile: params[:mobile], _type: type).last
		raise '请过段时间再验证，谢谢' if vcode && vcode.updated_at.since(5.minutes) > Time.now
		
		ValidCode.create(mobile: params[:mobile], _type: type)

		render json: {status: :success, msg: '发送成功'}

		rescue => e
			render json: {status: :failed, msg: e.message}
	end

	# 验证
	# 
	# Params
	# 	mobile: [String] 手机号
	# 	code: [String] 验证码
	# Return
	# 	status: [String] success
	# 	msg: [String] 验证成功
	# Error
	#   status: [String] failed
	#   msg: [String] msg_infos
	def valid
		vcode = ValidCode.where(mobile: params[:mobile], code: params[:code]).last
		if vcode.state
			vcode.update_attributes(state: false)
			render json: {status: :success, msg: '验证成功'}
		else
			render json: {status: :success, msg: '验证失败'}
		end
		rescue => e
			render json: {status: :failed, msg: e.message}
	end


end