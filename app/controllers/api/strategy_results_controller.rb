# 财务管理
class Api::StrategyResultsController < Api::BaseController
	before_filter :valid_current_user_role
	skip_before_filter :valid_permission
	before_filter :get_strategy_result, only: [:show, :update, :pay]

	# 提成列表
	# 
	# Params
	# 	access_token: [String] authenication_token
	# 	state: [String] false
	#   page: [Integer] 页码
	# Return
	# 	status: [String] success
	#   list: [Hash] strategyresults list
	#   total: [Integer] 总数
	# Error
	#   status: [String] failed
	def index
		conds = {strategy_id: @account.strategies.map(&:id)}
		conds[:state] = params[:state] == 'true' ? true : false

		strategy_results = StrategyResult.where(conds).page(params[:page]).order(updated_at: :desc)

		render json: {status: :success, list: strategy_results.map(&:to_hash), total: strategy_results.total_count}
	end

	# 更新提成
	# 
	# Params
	# 	access_token: [String] authenication_token
	#   strategy_result[saler_rate_amount]: [Float] 销售提成
	#   strategy_result[saler_director_rate_amount]: [Float] 销售主管提成
	#   strategy_result[introducer_rebate_amount]: [Float] 介绍人返利
	#   strategy_result[operator_amount]: [Float] 运营提成
	# 	strategy_result[cs_director_amount]: [Float] 售后提成
	#   strategy_result[planinger_director_amount]: [Float] 企划提成
	#
	# Return
	# 	status: [String] success
	#   msg: [String] 更新成功
	#   sr: [Hash] strategy_result_hash
	# Error
	#   status: [String] failed
	def update
		raise '此单已付过' if @sr.state
		
		if @sr.update_attributes(strategy_result_params)
			# gen log
			Logs::StrategyResult.create(method: 'update', p_hash: params, user_id: @current_user.id, strategy_result_id: @sr.id)
			render json: {status: :success, msg: '更新成功', sr: @sr.attributes.merge(order: @sr.order.to_hash)}
    else
    	render json: {status: :failed, msg: @sr.errors.messages.values.first}
		end
		rescue => e
			render json: {status: :failed, msg: e.message}
	end

	# 付款
	# 
	# Params
	# 	access_token: [String] authenication_token
	#
	# Return
	# 	status: [String] success
	#   msg: [String] 付款成功
	#   sr: [Hash] strategy_result_hash
	# Error
	#   status: [String] failed
	def pay
		raise '此单已付过' if @sr.state
		raise "余额不足，此单需支付#{@sr.total_amount}" if @sr.total_amount > @current_user.wallet_total

		StrategyResult.transaction do
			WalletLog.transaction do
				@sr.update_attributes!(state: true)
				# 销售提成
				# @sr.amount_opt(@sr.saler, @sr.saler_rate_amount, @current_user) if @sr.saler && @sr.saler_rate_amount > 0
				# 销售主管提成
				# @sr.amount_opt(@sr.saler_director, @sr.saler_director_rate_amount, @current_user) if @sr.saler_director && @sr.saler_director_rate_amount > 0	
				# 介绍人返利
				# @sr.amount_opt(@sr.introducer, @sr.introducer_rebate_amount, @current_user) if @sr.introducer && @sr.introducer_rebate_amount > 0
				# 运营提成
				# @sr.amount_opt(@sr.operator, @sr.operator_amount, @current_user) if @sr.operator && @sr.operator_amount > 0
				# 售后提成
				# @sr.amount_opt(@sr.cs_director, @sr.cs_director_amount, @current_user) if @sr.cs_director && @sr.cs_director_amount > 0
				# 企划提成
				# @sr.amount_opt(@sr.planinger_director, @sr.planinger_director_amount, @current_user) if @sr.planinger_director && @sr.planinger_director_amount > 0
				# 
				StrategyResult::ROLE_HASH.each do |k, v|
          @sr.amount_opt(@sr.send(k), @sr.send(v).to_f, @current_user) if @sr.send(k) && @sr.send(v).to_f > 0
				end
				# gen log
				Logs::StrategyResult.create!(method: 'pay', p_hash: params, user_id: @current_user.id, strategy_result_id: @sr.id)

				render json: {status: :success, msg: '支付成功'}
			end
		end

		rescue => e
			render json: {status: :failed, msg: e.message}
	end

	# 单个提成详情
	# 
	# Params
	# 	access_token: [String] authenication_token
	# 	id: [Integer] ID
	# Return
	# 	status: [String] success
	#   
	# Error
	#   status: [String] failed
	def show
		render json: {status: :success, sr: @sr.attributes.merge(order: @sr.order.to_hash)}
	end

	private

	def valid_current_user_role
		raise '无权限访问' unless %w(acct acct_director).include?(@current_user.role)

		rescue => e
			render json: {status: :failed, msg: e.message}
	end

	def get_strategy_result
		@sr = StrategyResult.includes(:order).find(params[:id])
		raise '没有该条记录' unless @sr
		rescue => e
			render json: {status: :failed, msg: e.message}
	end

	def strategy_result_params
		params[:strategy_result].permit(:saler_rate_amount, :saler_director_rate_amount, :introducer_rebate_amount, :operator_amount, :cs_director_amount, :planinger_director_amount)
	end

end
