# 投诉 反馈
class Api::SuggestionsController < Api::BaseController
	skip_before_filter :valid_permission

	# 新建
	#
	# Params
	# 	access_token: [String] authenication_token
	# 	suggestion[_type]: [Integer] 1反馈，2投诉, default is 1
	#   suggestion[content]: [String] 内容
	# 	suggestion[c_name]: [String] 客户姓名
	# 	suggestion[c_tel]: [String] 客户手机号
	# Return
	# 	status: [String] success
	# 	msg: [String] 创建成功
	# Error
	#   status: [String] failed
	#   msg: [String] msg_infos
	def create
		suggestion = @current_user.suggestions.new(suggestion_params)
		suggestion._type = params[:suggestion][:_type] == '2' ? 2 : 1

		if suggestion.save
			render json: {status: :success, msg: '创建成功'}
    else
    	render json: {status: :failed, msg: suggestion.errors.messages.values.first}
		end

	end

	private

	def suggestion_params
		params[:suggestion].permit(:_type, :content, :c_name, :c_tel)
	end

end