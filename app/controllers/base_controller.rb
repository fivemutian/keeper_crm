class BaseController < ApplicationController
	include SessionsHelper

	before_filter :valid_current_user

	private

	def valid_current_user
		redirect_to :login unless logged_in?
	end

end