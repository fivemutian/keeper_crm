module Admin
  class WalletLogsController < Admin::ApplicationController
    before_filter :get_wlog, only: [:confirm_pay]
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = WalletLog.withdraw.order(updated_at: :desc).page(params[:page]).per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   WalletLog.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information

    def confirm_pay
        redirect_to(admin_wallet_logs_path, notice: '此单已确认付过款') if @wlog.try(:paid)
      if @wlog.update_attributes(paid: true)
        redirect_to admin_wallet_logs_path, notice: '已确认付款'
      else
        redirect_to admin_wallet_logs_path, notice: '操作失败请重新操作'
      end
    end

    private

    def get_wlog
      @wlog = WalletLog.find_by_id(params[:id])
    end
  end
end
