module Admin
  class OrdersController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    def index
      super
      @resources = Order.
        page(params[:page]).
        per(10)
    end

    def update
      order = Order.find(params[:id])
      order.update_attributes(order_params)

      redirect_to :admin_order
    end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   Order.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information

    private

    def order_params
      params[:order].permit(:user_id, :region_id, :store_id, :expected_square, :booking_date, :estimated_total)
    end
  end
end
