# == Schema Information
#
# Table name: accounts
#
#  id          :integer          not null, primary key
#  name        :string           not null
#  code        :string           not null
#  type        :string           not null
#  reg_address :string
#  address     :string
#  company_ids :integer          is an Array
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  parent_id   :integer
#  logo        :string
#  cgj_id      :integer
#

class Company < Account

  belongs_to :parent, class_name: 'Account'

  def cs_director
    users.find_by_role 'cs_director'
  end

  def menu
    {
      admin:          {customers: '客户', orders: '订单', clues: '线索', stores: '渠道/门店', users: '用户', strategies: '策略', accounts: '品牌审核'},
      saler_director: {customers: '客户', orders: '订单', clues: '线索'},
      saler:          {customers: '客户', orders: '订单', clues: '线索'},
      acct:           {orders: '订单'},
      acct_director: {},
      operator: {},
      operator_director: {},
      cs: {},
      cs_director: {},
      planinger: {},
      planinger_director: {},
      introducer: {}
    }
  end

  def sync_cgj
    res = Cgj.create_company(cgj_hash)
    _hash = JSON res.body

    if _hash["code"] == 200
      manager = _hash["result"]["manager_info"]
      if manager
        self.update_attributes(cgj_id: _hash["result"]["id"])
        self.admin.update_attributes(cgj_user_id: manager["id"])
      else
        self.update_attributes(cgj_id: _hash["result"]["company_id"])
        self.admin.update_attributes(cgj_user_id: _hash["result"]["id"])
      end
    end
  end

	def co_applies
		Apply.where(resource_name: 'Company', resource_id: id, _action: "cooperate", state: 0).map(&:cooperate_hash)
	end

  def cgj_hash
    {
      id:            cgj_id,
      name:          name,
      address:       address,
      account_id:    parent.cgj_id,
      logo:          logo_url,
      user:          admin.cgj_hash,
      account:       parent.cgj_hash,
      _account_user: parent.admin.cgj_hash
    }
  end

  def select_hash
    {
      id: id,
      name: name,
      logo: logo_url,
      cgj_id: cgj_id,
      stores_tree: stores_tree
    }
  end
	
end
