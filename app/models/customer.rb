# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  tel        :string           not null
#  province   :string
#  city       :string
#  area       :string
#  street     :string
#  address    :string
#  longitude  :string
#  latitude   :string
#  remark     :string
#  user_id    :integer          not null
#  account_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Customer < ActiveRecord::Base

	validates_uniqueness_of :tel, message: '手机号已被使用'

	belongs_to :account
	belongs_to :user
	has_many :orders

	private

	def self.ransortable_attributes(auth_object = nil)
    column_names
  end

  def self.ransackable_attributes(auth_object = nil)
    ransortable_attributes + _ransackers.keys
  end
	
	ransacker :customer_info do |parent|
    Arel::Nodes::InfixOperation.new('||',
      Arel::Nodes::InfixOperation.new('||',
        parent.table[:tel], Arel::Nodes.build_quoted(' ')),
      parent.table[:name])
  end
end
