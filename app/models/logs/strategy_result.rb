# == Schema Information
#
# Table name: logs_strategy_results
#
#  id                 :integer          not null, primary key
#  method             :string           not null
#  p_hash             :jsonb            not null
#  user_id            :integer          not null
#  strategy_result_id :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Logs::StrategyResult < ActiveRecord::Base
	validates :method, inclusion: {in: ['update', 'pay'], message: '不在所选范围之内'} 
end
