# == Schema Information
#
# Table name: logs_orders
#
#  id         :integer          not null, primary key
#  method     :string           not null
#  p_hash     :jsonb            not null
#  user_id    :integer          not null
#  order_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Logs::Order < ActiveRecord::Base

	validates :method, inclusion: {in: ['update_estimated_total', 'update_rebate'], message: '不在所选范围之内'} 

end
