# == Schema Information
#
# Table name: applies
#
#  id             :integer          not null, primary key
#  target_user_id :integer          not null
#  resource_name  :string           not null
#  resource_id    :integer          not null
#  _action        :string
#  state          :integer          default(0)
#  remark         :string
#  user_id        :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Apply < ActiveRecord::Base

	belongs_to :user

	STATE = {
						0  => '等待审核',
						1  => '通过',
						-1 => '拒绝'
					}

	# _action: 合作 cooperate

	def target_user
		User.find target_user_id
	end


	def cooperate_hash
		{
			id: id,
			dealer_name: user.account.name,
			dealer_admin: user.name,
			dealer_mobile: user.mobile,
			state: STATE[state]
		}
	end

	def resource
		Company.find resource_id
	end

	def wait_hash
		{
			id: 					resource_id,
			name: 				resource.name,
			admin_name: 	resource.admin.try(:name),
			admin_mobile: resource.admin.try(:mobile),
			state: STATE[state]
		}
	end
end
