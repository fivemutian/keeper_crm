# == Schema Information
#
# Table name: permissions
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  _controller_action :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Permission < ActiveRecord::Base
	has_and_belongs_to_many :users
end
