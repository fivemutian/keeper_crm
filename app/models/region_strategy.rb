# == Schema Information
#
# Table name: region_strategies
#
#  id         :integer          not null, primary key
#  store_rate :float            default(0.0)
#  plan_rate  :float            default(0.0)
#  region_id  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  store_id   :integer
#

class RegionStrategy < ActiveRecord::Base

	belongs_to :store
end
