# == Schema Information
#
# Table name: pictures
#
#  id               :integer          not null, primary key
#  image            :string
#  image_type       :string
#  pictureable_id   :integer
#  pictureable_type :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Picture < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  belongs_to :pictureable, polymorphic: true


  def url
    self.image.try(:url)
  end
  
  def uid
    rand(1...1000)
  end

  def as_json
    super({methods: [:url, :uid]})
  end

end
