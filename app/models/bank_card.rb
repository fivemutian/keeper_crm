# == Schema Information
#
# Table name: bank_cards
#
#  id         :integer          not null, primary key
#  code       :string           not null
#  account    :string           not null
#  branch     :string           not null
#  name       :string           not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BankCard < ActiveRecord::Base

	belongs_to :user
end
