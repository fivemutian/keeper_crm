# == Schema Information
#
# Table name: suggestions
#
#  id            :integer          not null, primary key
#  _type         :integer          default(1)
#  content       :string
#  state         :string           default("suspending")
#  c_name        :string
#  c_tel         :string
#  remark        :string
#  transactor_id :integer
#  user_id       :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Suggestion < ActiveRecord::Base

	belongs_to :user
	belongs_to :transactor, class_name: 'User', foreign_key: 'transactor_id'

	# state: suspending, handing, solved

end
