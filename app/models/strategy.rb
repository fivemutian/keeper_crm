# == Schema Information
#
# Table name: strategies
#
#  id                  :integer          not null, primary key
#  start_at            :datetime
#  end_at              :datetime
#  product_id          :integer
#  rate                :float
#  state               :boolean          default(TRUE)
#  user_id             :integer
#  account_id          :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  title               :string
#  discount            :float
#  rebate              :float
#  province            :string
#  city                :string
#  area                :string
#  region_id           :integer
#  pound               :integer
#  saler_director_rate :float            default(0.0)
#  cs_rate             :float            default(0.0)
#  store_id            :integer
#

class Strategy < ActiveRecord::Base

	# discount 						折扣 客户
	# rebate   						返利 介绍人
	# rate     						提成 销售
	# saler_director_rate 提成 销售主管
	# cs_rate	 						提成 客服
	
	# 基数  1, 订单总价税前； 2, 订单总价税后，3, 客服提成

	belongs_to :account

	scope :valid, lambda {where("state=true and start_at<? and end_at>?", Time.now, Time.now)}
end
