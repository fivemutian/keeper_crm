class CreateLogsStrategyResults < ActiveRecord::Migration
  def change
    create_table :logs_strategy_results do |t|
    	t.string :method, null: false
    	t.jsonb :p_hash,  null: false
    	t.references :user,  null: false
    	t.references :strategy_result, null: false

      t.timestamps null: false
    end
  end
end
