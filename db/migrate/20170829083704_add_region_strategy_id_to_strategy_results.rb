class AddRegionStrategyIdToStrategyResults < ActiveRecord::Migration
  def change
  	add_column :strategy_results, :region_strategy_id, :integer
  end
end
