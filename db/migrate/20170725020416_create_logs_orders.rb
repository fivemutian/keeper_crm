class CreateLogsOrders < ActiveRecord::Migration
  def change
    create_table :logs_orders do |t|
    	t.string :method, null: false
    	t.jsonb :p_hash,  null: false
    	t.references :user,  null: false
    	t.references :order, null: false
    	
      t.timestamps null: false
    end
  end
end
