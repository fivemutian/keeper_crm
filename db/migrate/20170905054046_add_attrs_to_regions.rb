class AddAttrsToRegions < ActiveRecord::Migration
  def change
  	add_column :regions, :user_id, :integer
  	add_column :stores, :user_id, :integer
  end
end
