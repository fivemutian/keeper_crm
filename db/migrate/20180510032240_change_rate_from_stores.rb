class ChangeRateFromStores < ActiveRecord::Migration
  def change
    remove_column :stores, :rate
    add_column :stores, :rate, :float, default: 0.0
  end
end
