class AddStoreIdToRegionStrategies < ActiveRecord::Migration
  def change
    add_column :region_strategies, :store_id, :integer
    add_index :region_strategies, :store_id
  end
end
