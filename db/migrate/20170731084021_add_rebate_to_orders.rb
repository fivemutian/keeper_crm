class AddRebateToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :rebate, :float, default: 0.02
  end
end
