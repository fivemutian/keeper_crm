class AddStateToStrategyResults < ActiveRecord::Migration
  def change
  	add_column :strategy_results, :state, :boolean, default: false
  	add_column :wallet_logs, :paid, :boolean, default: false
  end
end
