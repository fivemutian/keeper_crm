class AddStoreIdToStrategy < ActiveRecord::Migration
  def change
    add_column :strategies, :store_id, :integer
    add_index :strategies, :store_id
  end
end
