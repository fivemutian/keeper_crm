class CreateSuggestions < ActiveRecord::Migration
  def change
    create_table :suggestions do |t|
    	t.integer :_type, default: 1
    	t.string :content
    	t.string :state, default: 'suspending'
    	t.string :c_name
    	t.string :c_tel
    	t.string :remark
    	t.integer :transactor_id

    	t.references :user, null: false

      t.timestamps null: false
    end
  end
end
