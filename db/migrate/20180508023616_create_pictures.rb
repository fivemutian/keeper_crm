class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :image
      t.string :image_type
      t.belongs_to :pictureable, polymorphic: true
      t.timestamps null: false
    end
  end
end
