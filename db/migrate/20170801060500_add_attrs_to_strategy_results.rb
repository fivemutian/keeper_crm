class AddAttrsToStrategyResults < ActiveRecord::Migration
  def change
  	add_column :strategy_results, :saler_director_id, :integer
  	add_column :strategy_results, :saler_director_rate_amount, :float
  end
end
