class AddIsSyncToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :is_sync, :boolean, default: false
  end
end
