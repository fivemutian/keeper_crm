class CreateRegionStrategies < ActiveRecord::Migration
  def change
    create_table :region_strategies do |t|
    	t.float :store_rate, default: 0.0
    	t.float :plan_rate,  default: 0.0

    	t.references :region, null: false

      t.timestamps null: false
    end
  end
end
