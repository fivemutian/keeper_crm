class AddSomesToStrategyResults < ActiveRecord::Migration
  def change
  	add_column :strategies, :saler_director_rate, :float, default: 0.0
  	add_column :strategies, :cs_rate, 						:float, default: 0.0

  	add_column :strategy_results, :cs_director_id, :integer
  	add_column :strategy_results, :cs_director_amount, :float
  	add_column :strategy_results, :operator_id, :integer
  	add_column :strategy_results, :operator_amount, :float
  	add_column :strategy_results, :planinger_director_id, :integer
  	add_column :strategy_results, :planinger_director_amount, :float
  end
end
