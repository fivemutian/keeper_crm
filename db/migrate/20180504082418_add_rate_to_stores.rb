class AddRateToStores < ActiveRecord::Migration
  def change
    add_column :stores, :rate, :integer, default: 0
  end
end
