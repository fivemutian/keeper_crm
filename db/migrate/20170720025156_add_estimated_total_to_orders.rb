class AddEstimatedTotalToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :estimated_total, :float, default: 0.0
  end
end
