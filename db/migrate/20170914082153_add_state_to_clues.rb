class AddStateToClues < ActiveRecord::Migration
  def change
  	add_column :clues, :state, :boolean, default: true
  end
end
