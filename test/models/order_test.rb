# == Schema Information
#
# Table name: orders
#
#  id                             :integer          not null, primary key
#  uuid                           :string
#  width                          :integer
#  height                         :integer
#  expected_square                :float            default(0.0)
#  booking_date                   :datetime         not null
#  rate                           :float
#  total                          :float
#  remark                         :string
#  state                          :string
#  courier_number                 :string
#  install_date                   :datetime
#  cgj_company_id                 :integer
#  cgj_customer_id                :integer
#  cgj_facilitator_id             :integer
#  cgj_customer_service_id        :integer
#  material                       :string
#  material_id                    :integer
#  workflow_state                 :string
#  public_order                   :boolean
#  square                         :float
#  mount_order                    :boolean
#  serial_number                  :string
#  is_company                     :boolean
#  measure_amount                 :float
#  install_amount                 :float
#  manager_confirm                :boolean
#  region                         :string           default("CRM")
#  terminal_count                 :float
#  amount_total_count             :float
#  basic_order_tax                :integer
#  measure_amount_after_comment   :integer
#  installed_amount_after_comment :integer
#  measure_comment                :integer
#  measure_raty                   :float
#  installed_raty                 :float
#  service_measure_amount         :float
#  service_installed_amount       :float
#  basic_tax                      :float
#  deduct_installed_cost          :float
#  deduct_measure_cost            :float
#  sale_commission                :integer
#  intro_commission               :integer
#  user_id                        :integer          not null
#  account_id                     :integer          not null
#  customer_id                    :integer          not null
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  introducer_name                :string
#  introducer_tel                 :string
#  introducer_id                  :integer
#  province                       :string
#  city                           :string
#  area                           :string
#  region_id                      :string
#  store_id                       :string
#  estimated_total                :float            default(0.0)
#  rebate                         :float            default(0.02)
#  measure_master                 :string
#  is_sync                        :boolean          default(FALSE)
#

require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
